﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class gameManager : MonoBehaviour {
    public Text scoreText, lifeText, gameOverText;
    public GameObject gameoverMenu;
    public int totalLife;
    private int totalScore;
    void Start() {
        scoreText.text = "Score " + totalScore;
        lifeText.text = "Lives " + totalLife;
        gameoverMenu.SetActive(false);
    }

    public void UpdateLifeText(int life) {
        totalLife += life;
        lifeText.text = "Lives " + totalLife;
    }

    public void UpdateScoreText(int score) {
        totalScore += score;
        scoreText.text = "Score " + totalScore;
    }

    public void GameOver() {
        gameoverMenu.SetActive(true);
        if (totalScore < userData.score) {
            gameOverText.text = "GAMEOVER!! >:) SCORE: " + totalScore;
        } else {
            gameOverText.text = "GAMEOVER!! >:) SCORE: " + totalScore + " NEW HIGHSCORE!!";
            userData.score = totalScore;
            StartCoroutine(saveScore());
        }
        Time.timeScale = 0;
    }
    public void backToMenu() {
        SceneManager.LoadScene("homePage", LoadSceneMode.Single);
        Time.timeScale = 1;
    }
    private IEnumerator saveScore()
    {
        string unityPassword = "Darren Cosgrave";
        string scoreURL = "http://darrencosgraveproject.co.nf/include/unityAccess/unityUpdateScore.php";
        WWWForm scoreForm = new WWWForm();
        scoreForm.AddField("username", userData.username);
        scoreForm.AddField("score", totalScore);
        scoreForm.AddField("unityPassword", unityPassword);

        WWW scoreWWW = new WWW(scoreURL, scoreForm);
        yield return scoreWWW;
        print("Score saved");
    }
}
