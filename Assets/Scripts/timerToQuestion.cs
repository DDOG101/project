﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class timerToQuestion : MonoBehaviour {
    public float timeLeft = 60f;
    private float copyTimeLeft;
    public GameObject questionLayout, questionAnswer, questionLayoutInput;
    public GameObject background;
    public Text timeText, questionText, ans1, ans2, ans3, ans4, questionValue, bonus;
    public Text inputQuestionText;
    public InputField inputAns;
    public string[] questions;
    public string[] inputQuestions;
    private int onQuestion = 0, onQuestionInput = 0;
    private string[] questionItems, questionItemsInput;
    private string[] questionParts= new string[7];
    private string[] questionPartsInput = new string[4];
    private int amountOfQuestions, amountOfQuestionsInput;
    private bool finishedQuestion = true;
    private string rightAnswer, questionTag;
    public float mathQuestionRight, totalMathQuestionsAsked, englishQuestionRight, totalEnglishQuestionsAsked, totalQuestionsRight, totalQuestionsAsked;
    private gameManager gm;
    private playerShipControl playerControl;
    private float speedIncress=1.5f, fireSpeed=0.5f;
    private string typeQuestion;
    public int totalQuestions, currentQuestion;
    void OnDestroy()
    {
        overAllResult();
        enemyMove.xMovementPerSecond = -2f;
        enemyFire.FIRE_DELAY = 4f;
        enemyFire.projectileSpeed = 200f;
    }
    void Start () {
        background.SetActive(false);
        timeText.text = timeLeft.ToString();
        StartCoroutine(loadInputQuestion());
        StartCoroutine(loadMcqQuestion());
        copyTimeLeft = timeLeft;

        GameObject gameManager = GameObject.FindWithTag("MainCamera");
        gm = gameManager.GetComponent<gameManager>();

        GameObject player = GameObject.FindWithTag("Player");
        playerControl = player.GetComponent<playerShipControl>();
        Image back = background.GetComponent<Image>();
        Color c = back.color;
        c.a = 0.4f;
        back.color = c;
    }
	
	void Update () {
        timeLeft -= Time.deltaTime;
        if (timeLeft<0 && finishedQuestion==true){
            totalQuestions = amountOfQuestions + amountOfQuestionsInput;

            Time.timeScale = 0;
            finishedQuestion = false;
            int random = Random.Range(1, 3);

            if (random == 1) {
                sortData(onQuestion);
                setQuestionLayout();
                typeQuestion = "mcq";
            } else {
                sortDataInput(onQuestionInput);
                setQuestionLayoutInput();
                typeQuestion = "input";
            }
        } else{
            timeText.text = "Time " + Mathf.Round(timeLeft).ToString();
        }
	}
    private IEnumerator loadInputQuestion() {
        WWWForm form = new WWWForm();
        form.AddField("unityPassword", "Darren Cosgrave");

        WWW questionWWW = new WWW("http://darrencosgraveproject.co.nf/include/unityAccess/unityInputQuestions.php", form);
        yield return questionWWW;
        string msg = questionWWW.text;
        print("web response: " + msg);
        msg = msg.Substring(0, msg.Length - 1);
        inputQuestions = msg.Split(';');
        amountOfQuestionsInput = inputQuestions.Length;
        for (int i = 0; i < inputQuestions.Length; i++)
        {
            string temp = inputQuestions[i];
            int r = Random.Range(i, inputQuestions.Length);
            inputQuestions[i] = inputQuestions[r];
            inputQuestions[r] = temp;
        }
    }

    private void sortDataInput(int index)
    {
        string questionPicked = inputQuestions[index];
        questionItemsInput = questionPicked.Split('|');
        questionPartsInput[0] = sortItems(questionItemsInput[0], "ID:");
        questionPartsInput[1] = sortItems(questionItemsInput[1], "Tag:");
        questionPartsInput[2] = sortItems(questionItemsInput[2], "Question:");
        questionPartsInput[3] = sortItems(questionItemsInput[3], "RightAnswer:");
    }

    private void setQuestionLayoutInput()
    {
        reset();
        questionTag = questionPartsInput[1];
        inputQuestionText.text = questionPartsInput[2];
        rightAnswer = questionPartsInput[3];
        questionLayoutInput.SetActive(true);

        totalQuestionsAsked++;
        if (questionTag == "math") {
            totalMathQuestionsAsked++;
        } if (questionTag == "english") {
            totalEnglishQuestionsAsked++;
        }
        background.SetActive(true);
    }

    public void submit() {
        if (inputAns.text.ToString() == rightAnswer) {
            questionValue.text = "Correct";
            correctAns();
        } else {
            questionValue.text = "Wrong: the right answer was: '" + rightAnswer+"'";
            bonus.text = "You gained nothing!! >:)";
        }
        questionLayoutInput.SetActive(false);
        questionAnswer.SetActive(true);
    }
    public void reset() {
        inputAns.text = "";
    }

    private IEnumerator loadMcqQuestion() {
        WWWForm form = new WWWForm();
        form.AddField("unityPassword", "Darren Cosgrave");

        WWW questionWWW = new WWW("http://darrencosgraveproject.co.nf/include/unityAccess/unityMcq_Questions.php", form);
        yield return questionWWW;
        string msg = questionWWW.text;
        print("web response: " + msg);
        msg = msg.Substring(0, msg.Length - 1);
        questions = msg.Split(';');
        amountOfQuestions = questions.Length;

        for (int i = 0; i < questions.Length; i++) {
            string temp = questions[i];
            int r = Random.Range(i, questions.Length);
            questions[i] = questions[r];
            questions[r] = temp;
        }
    }

    private void sortData(int index) {
        string questionPicked = questions[index];
        questionItems = questionPicked.Split('|');
        questionParts[0] = sortItems(questionItems[0], "ID:");
        questionParts[1] = sortItems(questionItems[1], "Tag:");
        questionParts[2] = sortItems(questionItems[2], "Question:");
        questionParts[3] = sortItems(questionItems[3], "Answer1:");
        questionParts[4] = sortItems(questionItems[4], "Answer2:");
        questionParts[5] = sortItems(questionItems[5], "Answer3:");
        questionParts[6] = sortItems(questionItems[6], "RightAnswer:");
    }

    private string sortItems(string data, string index) {
        string value = data.Substring(data.IndexOf(index) + index.Length);

        return value;
    }

    private void setQuestionLayout() {
        questionTag = questionParts[1];
        questionText.text = "" + questionParts[2];

        rightAnswer = questionParts[6];

        for (int i = 3; i < questionParts.Length; i++)
        {
            string temp = questionParts[i];
            int r = Random.Range(i, questionParts.Length);
            questionParts[i] = questionParts[r];
            questionParts[r] = temp;
        }

        ans1.text = "" + questionParts[3];
        ans2.text = "" + questionParts[4];
        ans3.text = "" + questionParts[5];
        ans4.text = "" + questionParts[6];
        questionLayout.SetActive(true);
        background.SetActive(true);
        totalQuestionsAsked++;
        if (questionTag == "math") {
            totalMathQuestionsAsked++;
        } if (questionTag == "english") {
            totalEnglishQuestionsAsked++;
        }
    }

    private void correctAns()
    {
        if (questionTag == "math") {
            mathQuestionRight++;
        } if (questionTag == "english") {
            englishQuestionRight++;
        }
        totalQuestionsRight++;
        int random = Random.Range(1, 4);
        if (random == 1) {
            bonus.text = "You gained one life!!";
            gm.UpdateLifeText(1);
        } else if (random == 2) {
            bonus.text = "You gained incressed fire speed!!";
            StartCoroutine(incressFireSpeed());
        } else if (random == 3) {
            bonus.text = "You gained incressed movement speed!!";
            StartCoroutine(incressMoveSpeed());
        }
    }

    private IEnumerator incressFireSpeed() {
        playerControl.setFireTime(-fireSpeed);
        yield return new WaitForSeconds(copyTimeLeft / 2);
        playerControl.setFireTime(fireSpeed);
    }

    private IEnumerator incressMoveSpeed() {
        playerControl.setMoveSpeed(speedIncress);
        yield return new WaitForSeconds(copyTimeLeft / 2);
        playerControl.setMoveSpeed(-speedIncress);
    }

    public void ans1btn() {
        if (ans1.text.ToString() == rightAnswer) {
            questionValue.text = "Correct";
            correctAns();
        } else {
            questionValue.text = "Wrong";
            bonus.text = "You gained nothing!! >:)";
        }
        questionLayout.SetActive(false);
        questionAnswer.SetActive(true);
    }

    public void ans2btn() {
        if (ans2.text.ToString() == rightAnswer) {
            questionValue.text = "Correct";
            correctAns();
        } else {
            questionValue.text = "Wrong";
            bonus.text = "You gained nothing!! >:)";
        }
        questionLayout.SetActive(false);
        questionAnswer.SetActive(true);
    }

    public void ans3btn() {
        if (ans3.text.ToString() == rightAnswer) {
            questionValue.text = "Correct";
            correctAns();
        } else {
            questionValue.text = "Wrong";
            bonus.text = "You gained nothing!! >:)";
        }
        questionLayout.SetActive(false);
        questionAnswer.SetActive(true);
    }

    public void ans4btn() {
        if (ans4.text.ToString() == rightAnswer) {
            questionValue.text = "Correct";
            correctAns();
        } else {
            questionValue.text = "Wrong";
            bonus.text = "You gained nothing!! >:)";
        }
        questionLayout.SetActive(false);
        questionAnswer.SetActive(true);
    }

    public void resumeGame() {
        Time.timeScale = 1;
        timeLeft = copyTimeLeft;
        finishedQuestion = true;
        if (typeQuestion == "mcq") {
            if (onQuestion == amountOfQuestions - 1) {
                print("Reached the end of the questions go back to start again");
                onQuestion = 0;
            } else {
                onQuestion++;
                //TODO Mess with this
                if (enemyMove.xMovementPerSecond < -5) {
                    enemyMove.xMovementPerSecond += -0.5f;
                } if (enemyFire.FIRE_DELAY > 2) {
                    enemyFire.FIRE_DELAY += -0.5f;
                } if (enemyFire.projectileSpeed < 400) {
                    enemyFire.projectileSpeed += 100.0f;
                }
            }
        }

        if (typeQuestion == "input") {
            if (onQuestionInput == amountOfQuestionsInput - 1) {
                print("Reached the end of the questions go back to start again");
                onQuestionInput = 0;
            } else {
                onQuestionInput++;
                //TODO Mess with this
                if (enemyMove.xMovementPerSecond < -5) {
                    enemyMove.xMovementPerSecond += -0.5f;
                } if (enemyFire.FIRE_DELAY > 2) {
                    enemyFire.FIRE_DELAY += -0.5f;
                } if (enemyFire.projectileSpeed < 400) {
                    enemyFire.projectileSpeed += 100.0f;
                }
            }
        }

        currentQuestion++;
        if ((totalQuestions/2)<=currentQuestion) {
            this.GetComponent<spawnEnemyType2>().enabled = true;
        }

        questionAnswer.SetActive(false);
        background.SetActive(false);
        overAllResult();
        StartCoroutine(savePrecent());
    }

    public void overAllResult() {
        userData.setResults(mathQuestionRight, totalMathQuestionsAsked, englishQuestionRight, totalEnglishQuestionsAsked, totalQuestionsRight, totalQuestionsAsked);
    }

    private IEnumerator savePrecent()
    {
        string totalPrecent = userData.getOverAllPrecent().ToString(),
            mathPrecent = userData.getMathPrecent().ToString(),
            englishPrecent = userData.getEnglishPrecent().ToString();

        string unityPassword = "Darren Cosgrave";
        string precentURL = "http://darrencosgraveproject.co.nf/include/unityAccess/unityPrecentage.php";
        WWWForm precentForm = new WWWForm();
        precentForm.AddField("username", userData.username);
        precentForm.AddField("totalPrecent", totalPrecent);
        precentForm.AddField("mathPrecent", mathPrecent);
        precentForm.AddField("englishPrecent", englishPrecent);
        precentForm.AddField("mathQuestionRight", userData.mathQuestionRight.ToString());
        precentForm.AddField("totalMathQuestionsAsked", userData.totalMathQuestionsAsked.ToString());
        precentForm.AddField("englishQuestionRight", userData.englishQuestionRight.ToString());
        precentForm.AddField("totalEnglishQuestionsAsked", userData.totalEnglishQuestionsAsked.ToString());
        precentForm.AddField("totalQuestionsRight", userData.totalQuestionsRight.ToString());
        precentForm.AddField("totalQuestionsAsked", userData.totalQuestionsAsked.ToString());
        precentForm.AddField("unityPassword", unityPassword);

        WWW precentWWW = new WWW(precentURL, precentForm);
        yield return precentWWW;
        print("Precent saved");
    }
}