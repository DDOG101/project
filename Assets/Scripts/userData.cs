﻿public class userData {
    static public int score = 0;
    static public float soundLevel = 50.0f;
    static public bool soundOnOrOff = false;
    static public string username = "", password = "";
    static private float overAllResult, overAllMaths, overAllEnglish;
    static public float mathQuestionRight, totalMathQuestionsAsked, englishQuestionRight, totalEnglishQuestionsAsked, totalQuestionsRight, totalQuestionsAsked;

    static public void setResults(float totalMathQRight, float totalMathQAsked, float totalEnglishQRight, float totalEnglishQAsked, float totalQRight, float totalQAsked) {
        mathQuestionRight = totalMathQRight;
        totalMathQuestionsAsked = totalMathQAsked;
        englishQuestionRight = totalEnglishQRight;
        totalEnglishQuestionsAsked = totalEnglishQAsked;
        totalQuestionsRight = totalQRight;
        totalQuestionsAsked = totalQAsked;
    }

    static public float getMathPrecent() {
        overAllMaths = UnityEngine.Mathf.Round(mathQuestionRight / totalMathQuestionsAsked * 100);
        return overAllMaths;
    }

    static public float getEnglishPrecent() {
        overAllEnglish = UnityEngine.Mathf.Round(englishQuestionRight / totalEnglishQuestionsAsked * 100);
        return overAllEnglish;
    }

    static public float getOverAllPrecent() {
        overAllResult = UnityEngine.Mathf.Round(totalQuestionsRight / totalQuestionsAsked * 100);
        return overAllResult;
    }
}
