﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class homePage : MonoBehaviour {
    public GameObject settingsView,homepageView,resultView;
    public Slider volume;
    public userData userData;
    public AudioSource sound;
    public Text mathsText, englishText, overallText, resultText;
    void Start () {
        print("USERNAME: "+userData.username);
        print("SCORE: "+userData.score);
        if (PlayerPrefs.HasKey("soundLevel")){
            userData.soundLevel = PlayerPrefs.GetFloat("soundLevel");
            volume.value = userData.soundLevel;
            sound.mute = userData.soundOnOrOff;
            sound.volume = userData.soundLevel;
        }
    }

    public void spaceGame(){
        SceneManager.LoadScene("spaceGame", LoadSceneMode.Single);
    }

    public void results(){
        //TODO set if's for learing results
        if (userData.getMathPrecent().ToString()!="NaN" || userData.getEnglishPrecent().ToString() != "NaN") {
            mathsText.text = "Maths Precent: " + userData.getMathPrecent() + "%";
            englishText.text = "English Precent: " + userData.getEnglishPrecent() + "%";
            overallText.text = "Overall Precent: " + userData.getOverAllPrecent() + "%";
            if (userData.getOverAllPrecent() >= 80) {
                resultText.text = "Nice job!! :D";
            } else if (userData.getOverAllPrecent() >= 60) {
                resultText.text = "Almost there!! :D";
            } else if (userData.getOverAllPrecent() >= 40) {
                resultText.text = "Lets try harder this time!! :D";
            } else if (userData.getOverAllPrecent() < 40) {
                resultText.text = "Try again!! :D";
            }

            if (userData.totalMathQuestionsAsked <= 6) {
                mathsText.text = "Need to answer more maths questions";
            } else {
                if (userData.getMathPrecent() <= 10) {
                    mathsText.text = "Showing strong signs of Dyscalculia: " + userData.getMathPrecent() + "%";
                } else if (userData.getMathPrecent() <= 20) {
                    mathsText.text = "Maybe showing signs of Dyscalculia: " + userData.getMathPrecent() + "%";
                } else if (userData.getMathPrecent() <= 40) {
                    mathsText.text = "Need to practice more maths: " + userData.getMathPrecent() + "%";
                }
            }

            if (userData.totalEnglishQuestionsAsked <= 6) {
                englishText.text = "Need to answer more English questions";
            } else {
                if (userData.getEnglishPrecent() <= 10) {
                    englishText.text = "Showing strong signs of Dyslexia: " + userData.getEnglishPrecent() + "%";
                } else if (userData.getEnglishPrecent() <= 20) {
                    englishText.text = "Maybe showing signs of Dyslexia: " + userData.getEnglishPrecent() + "%";
                } else if (userData.getEnglishPrecent() <= 40) {
                    englishText.text = "Need to practice more English: " + userData.getEnglishPrecent() + "%";
                }
            }

            /*if (userData.getEnglishPrecent() < 40 && userData.totalEnglishQuestionsAsked <= 6) {
                englishText.text = "Need to answer more English questions";
            } else {
                englishText.text = "Maybe showing signs of Dyslexia";
            }*/
            //resultText.text = "Try again!! :D";
        } else {
            mathsText.text = "Please play the game";
            englishText.text = "to get results";
            overallText.text = " :D ";
            resultText.text = "";
        }
        resultView.SetActive(true);
        homepageView.SetActive(false);
    }

    public void settings(){
        settingsView.SetActive(true);
        homepageView.SetActive(false);
    }

    public void backHome(){
        homepageView.SetActive(true);
        settingsView.SetActive(false);
        resultView.SetActive(false);
    }

    public void checkboxChange(bool value){
        volume.interactable = value;
        if (value == true) { value = false; }
        else { value = true; }
        userData.soundOnOrOff = value;
        sound.mute = userData.soundOnOrOff;
    }

    public void volumeChange(){
        float soundLevel = volume.value;
        userData.soundLevel = soundLevel;
        PlayerPrefs.SetFloat("soundLevel", soundLevel);
        sound.volume = userData.soundLevel;
    }
}