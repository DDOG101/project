﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnEnemy : MonoBehaviour {
    public Rigidbody enemyPrefab;
    public float numberOfEmemys;
    public float nextSpawnTime;
    public float startWait;
    public float waveWait;

    void Start()
    {
        StartCoroutine(Spawn());
    }

    IEnumerator Spawn()
    {
        // waits to start the waves
        yield return new WaitForSeconds(startWait);

        while (true)
        {
            for (int i = 0; i < numberOfEmemys; i++)
            {
                // picks a random spot to spawn an enemy at 1 to 5
                int spawnLocation = Random.Range(1, 5);

                // enemy spawns at 1 of the 5 locations and
                // and gives some time till next spawn
                if (spawnLocation == 1)
                {
                    Vector3 spawnPosition = new Vector3(18, 0, 5);
                    Quaternion rotation = transform.rotation;
                    Rigidbody enemyRigidBody = (Rigidbody)Instantiate(enemyPrefab, spawnPosition, rotation);
                    enemyRigidBody.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
                    yield return new WaitForSeconds(nextSpawnTime);
                }

                else if (spawnLocation == 2)
                {
                    Vector3 spawnPosition = new Vector3(18, 0, 2.5f);
                    Quaternion rotation = transform.rotation;
                    Rigidbody enemyRigidBody = (Rigidbody)Instantiate(enemyPrefab, spawnPosition, rotation);
                    enemyRigidBody.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
                    yield return new WaitForSeconds(nextSpawnTime);
                }

                else if (spawnLocation == 3)
                {
                    Vector3 spawnPosition = new Vector3(18, 0, 0);
                    Quaternion rotation = transform.rotation;
                    Rigidbody enemyRigidBody = (Rigidbody)Instantiate(enemyPrefab, spawnPosition, rotation);
                    enemyRigidBody.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
                    yield return new WaitForSeconds(nextSpawnTime);
                }

                else if (spawnLocation == 4)
                {
                    Vector3 spawnPosition = new Vector3(18, 0, -2.5f);
                    Quaternion rotation = transform.rotation;
                    Rigidbody enemyRigidBody = (Rigidbody)Instantiate(enemyPrefab, spawnPosition, rotation);
                    enemyRigidBody.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
                    yield return new WaitForSeconds(nextSpawnTime);
                }

                else if (spawnLocation == 5)
                {
                    Vector3 spawnPosition = new Vector3(18, 0, -5);
                    Quaternion rotation = transform.rotation;
                    Rigidbody enemyRigidBody = (Rigidbody)Instantiate(enemyPrefab, spawnPosition, rotation);
                    enemyRigidBody.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
                    yield return new WaitForSeconds(nextSpawnTime);
                }
            }
            // after it spawns the enemies, it waits to start the next wave
            yield return new WaitForSeconds(waveWait);
        }
    }
}
