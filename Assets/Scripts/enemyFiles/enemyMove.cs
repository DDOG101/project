﻿using UnityEngine;

public class enemyMove : MonoBehaviour {
    public static float xMovementPerSecond = -2f;
	public float minX = -10;
	void Update () {
        moveEnemy();
	}

    private void moveEnemy() {
        float xc =+ xMovementPerSecond;
        float x = transform.position.x;

        xc *= Time.deltaTime;

        transform.Translate(xc, 0, 0);

        if (x < minX) {
            Destroy(gameObject);
        }
    }
}
