﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sideToSide : MonoBehaviour {
    public float zMovementPerSecond = 1f;

    public float maxZ;
    public float minZ;
    private bool movingSideToSide = false;

    void Update()
    {
        CheckLimits();
        MovingSideToSide(movingSideToSide);
    }

    void MovingSideToSide(bool no)
    {
        // dx is small amount of change to X position
        float dz = zMovementPerSecond;

        // if no = true
        if (no)
        {
            dz = -zMovementPerSecond;
        }

        // reduce DX based on fraction of a second since last frame
        dz *= Time.deltaTime;

        // move enemy by DX
        transform.Translate(0, 0, dz);
    }

    void CheckLimits()
    {
        float z = transform.position.z;
        // toggle motion if past top / bottom limit
        if (movingSideToSide == false && z > maxZ)
        {
            movingSideToSide = true;
            MovingSideToSide(movingSideToSide);
        }
        else if (z < minZ)
        {
            movingSideToSide = false;
            MovingSideToSide(movingSideToSide);
        }
    }
}
