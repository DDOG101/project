﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyFire : MonoBehaviour {

    // shortest time between firing another projectile
    public static float FIRE_DELAY = 4f;

    // ref to prefab of object to be fired and postion
    public Rigidbody projectilePrefab;
    public GameObject shotPostion;
    // change size of force (speed) given to projectile
    public static float projectileSpeed = 200f;

    // value Time.time must reach before next projectile can be fired
    private float nextFireTime = 2f;

    public AudioSource shotSound;
    public AudioClip shotClip;

    //--------------------------------
    // every frame check if fire key pressed (if past time to fire next projectile)
    void Update()
    {
        if (Time.time > nextFireTime)
        {
            CheckFireKey();
        }
    }

    //-----------------------------------
    // if fire pressed create projectile and update time for next time allowed to fire
    private void CheckFireKey()
    {
        float x = transform.position.x;
        // fire when gameObject is past 20 
        if (x < 14 && x > -2)
        {
            CreateProjectile();
        }
        // ensure a delay before next projectile can be fired
        nextFireTime = Time.time + FIRE_DELAY;
    }

    private void CreateProjectile()
    {
        Vector3 position = shotPostion.transform.position;//transform.position;
        Quaternion rotation = transform.rotation;
        Rigidbody projectileRigidBody = (Rigidbody)Instantiate(projectilePrefab, position, rotation);
        projectileRigidBody.rotation = Quaternion.Euler(90.0f, 0.0f, 90.0f);

        // create and apply velocity 
        // use TransformDirection() so direction is relative to current direction spawner is facing
        Vector3 projectileVelocity = transform.TransformDirection(Vector3.left * projectileSpeed);
        projectileRigidBody.AddForce(projectileVelocity);

        // destroy object after 5 seconds ...
        GameObject projectileGO = projectileRigidBody.gameObject;
        Destroy(projectileGO, 5f);
        shotSound.PlayOneShot(shotClip, 0.7f);
    }
}
