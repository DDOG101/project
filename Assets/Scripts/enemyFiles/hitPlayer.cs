﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hitPlayer : MonoBehaviour {
    private gameManager gm;
    public AudioClip player;
    public AudioSource sound;

    void Start()
    {
        GameObject gameManager = GameObject.FindWithTag("MainCamera");
        gm = gameManager.GetComponent<gameManager>();
    }

    IEnumerator OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == ("Player"))
        {
            gm.UpdateLifeText(-1);
            sound.PlayOneShot(player, 0.7f);
            GameObject playerObject = GameObject.FindGameObjectWithTag("Player");
            playerObject.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
            playerObject.transform.GetComponent<SphereCollider>().enabled = false;
            Vector3 position = new Vector3(0.0f, 0.0f, 0.0f);
            playerObject.transform.position = position;
            this.GetComponent<CapsuleCollider>().enabled = false;
            transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
            if (gm.totalLife == 0) {
                gm.GameOver();
            }
            yield return new WaitForSeconds(1);
            playerObject.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
            yield return new WaitForSeconds(1);
            playerObject.transform.GetComponent<SphereCollider>().enabled = true;
            Destroy(gameObject,5f);
            //ToDo Need to fix this!! Spawn in explosion and give player shild
        }
    }
}
