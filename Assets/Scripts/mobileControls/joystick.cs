﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class joystick : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{
    private Image bgImage;
    private Image joystickImg;

    public Vector3 inputDirection { set; get; }
    private void Start()
    {
        bgImage = GetComponent<Image>();
        joystickImg = transform.GetChild(0).GetComponent<Image>();
        inputDirection = Vector3.zero;

        Color c = bgImage.color;
        c.a = 0.4f;
        bgImage.color = c;
        //c.a = 0.8f;
        //joystickImg.color = c;
    }
    public virtual void OnDrag(PointerEventData ped)
    {
        Vector2 pos = Vector2.zero;
        if(RectTransformUtility.ScreenPointToLocalPointInRectangle(bgImage.rectTransform,ped.position,ped.pressEventCamera,out pos))
        {
            pos.x = (pos.x / bgImage.rectTransform.sizeDelta.x);
            pos.y = (pos.y / bgImage.rectTransform.sizeDelta.y);

            float x = (bgImage.rectTransform.pivot.x == 1) ? pos.x * 2 + 1 : pos.x * 2 - 1;
            float y = (bgImage.rectTransform.pivot.y == 1) ? pos.y * 2 + 1 : pos.y * 2 - 1;

            inputDirection = new Vector3(x,0,y);

            inputDirection = (inputDirection.magnitude > 1) ? inputDirection.normalized : inputDirection;

            joystickImg.rectTransform.anchoredPosition = new Vector3(inputDirection.x * (bgImage.rectTransform.sizeDelta.x / 3), inputDirection.z * (bgImage.rectTransform.sizeDelta.y / 3));
        }
    }

    public virtual void OnPointerDown(PointerEventData ped)
    {
        OnDrag(ped);
    }

    public virtual void OnPointerUp(PointerEventData ped)
    {
        inputDirection = Vector3.zero;
        joystickImg.rectTransform.anchoredPosition = Vector3.zero;
    }
}
