﻿using UnityEngine;
using UnityEngine.EventSystems;

public class fireButton : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
{
    public static bool pushed= false;

    public void OnPointerDown(PointerEventData eventData)
    {
        pushed = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        pushed = false;
    }
}