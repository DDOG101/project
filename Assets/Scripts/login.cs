﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class login : MonoBehaviour {
    public GameObject username;
    private string UsernameString;
    public GameObject password;
    private string PasswordString;
    public GameObject loginMenu, registerMenu;
    private string loginURL = "http://darrencosgraveproject.co.nf/include/unityAccess/unitylogin.php";
    private string registerURL = "http://darrencosgraveproject.co.nf/include/unityAccess/unityRegister.php";
    private string scoreURL = "http://darrencosgraveproject.co.nf/include/unityAccess/unityScore.php";
    public GameObject NewUser, NewPassword, RePassword, Email;
    private string newUser, newPassword, rePassword, email;
    private string unityPassword = "Darren Cosgrave";
    public GameObject loginError, registerError;
    public userData userData;
    void Start () {
        username.GetComponent<InputField>().Select();
        registerMenu.SetActive(false);
    }
	
	void Update () {
        if (Input.GetKeyDown(KeyCode.Tab)) {
            if (username.GetComponent<InputField>().isFocused) {
                password.GetComponent<InputField>().Select();
            } if (password.GetComponent<InputField>().isFocused) {
                username.GetComponent<InputField>().Select();
            } if (NewUser.GetComponent<InputField>().isFocused) {
                NewPassword.GetComponent<InputField>().Select();
            } if (NewPassword.GetComponent<InputField>().isFocused) {
                RePassword.GetComponent<InputField>().Select();
            } if (RePassword.GetComponent<InputField>().isFocused) {
                Email.GetComponent<InputField>().Select();
            } if (Email.GetComponent<InputField>().isFocused) {
                NewUser.GetComponent<InputField>().Select();
            }
        } if (Input.GetKeyDown(KeyCode.Return)) {
            StartCoroutine(Login());
        }
	}

    public void login_button() {
        StartCoroutine(Login());
    }

    private IEnumerator Login() {
        UsernameString = username.GetComponent<InputField>().text;
        PasswordString = password.GetComponent<InputField>().text;

        WWWForm form = new WWWForm();
        form.AddField("username", UsernameString);
        form.AddField("password", PasswordString);
        form.AddField("unityPassword", unityPassword);

        WWW loginAccountWWW = new WWW(loginURL, form);
        yield return loginAccountWWW;
        string msg = loginAccountWWW.text;
        print(msg);
        bool isOnDatabase = false;
        if (msg.Contains("username is in database")) {
            isOnDatabase = true;
        } else {
            isOnDatabase = false;
            Text textRef = loginError.GetComponent<Text>();
            textRef.text= "Username not found";
        } if (isOnDatabase) {
            userData.username = UsernameString;
            userData.password = PasswordString;

            WWWForm scoreForm = new WWWForm();
            scoreForm.AddField("username", userData.username);
            scoreForm.AddField("unityPassword", unityPassword);

            WWW scoreWWW = new WWW(scoreURL, scoreForm);
            yield return scoreWWW;
            string scoreText = scoreWWW.text;
            userData.score = int.Parse(scoreText);
            SceneManager.LoadScene("homePage", LoadSceneMode.Single);
        }
    }

    public void register_button() {
        Text textRef = GameObject.Find("SignIn").GetComponent<Text>();
        textRef.text = "Sign In";
        loginMenu.SetActive(false);
        registerMenu.SetActive(true);
        NewUser.GetComponent<InputField>().Select();
    }

    public void register() {
        StartCoroutine(addNewUser());
    }

    private IEnumerator addNewUser(){
        bool passwordMatch = false, emailVal = false, usernameVal = false;
        newUser = NewUser.GetComponent<InputField>().text;
        newPassword = NewPassword.GetComponent<InputField>().text;
        rePassword = RePassword.GetComponent<InputField>().text;
        email = Email.GetComponent<InputField>().text;
        Text textRef = registerError.GetComponent<Text>();
        registerError.SetActive(false);
        if (newUser != "") {
            usernameVal = true;
        } else {
            textRef.text = "Username not set";
            registerError.SetActive(true);
        } if (email.Contains("@")) {
            emailVal = true;
        } else {
            textRef.text = "EMAIL ERROR!!";
            registerError.SetActive(true);
        } if (newPassword == rePassword && newPassword!="") {
            passwordMatch = true;
        } else {
            textRef.text = "Passwords don't match";
            registerError.SetActive(true);
        } if (usernameVal == true && passwordMatch == true && emailVal == true) {
            WWWForm form = new WWWForm();
            form.AddField("username", newUser);
            form.AddField("password", newPassword);
            form.AddField("retypePassword", rePassword);
            form.AddField("email", email);
            form.AddField("unityPassword", unityPassword);
            WWW registerWWW = new WWW(registerURL, form);
            yield return registerWWW;
            
            string msg = registerWWW.text;
            print("html response: "+msg);
            if (msg.Contains("New user added")) {
                SceneManager.LoadScene("homePage", LoadSceneMode.Single);
            } else if (msg.Contains("Username taken")) {
                textRef.text = "USERNAME TAKEN!!";
                registerError.SetActive(true);
            }
        } else {
            print("Error with input");
        }
    }

    public void backToLogin(){
        registerError.SetActive(false);
        loginMenu.SetActive(true);
        registerMenu.SetActive(false);
        username.GetComponent<InputField>().Select();
    }
}