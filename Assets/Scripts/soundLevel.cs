﻿using UnityEngine;

public class soundLevel : MonoBehaviour {
    public AudioSource sound;
    void Start () {
        if (PlayerPrefs.HasKey("soundLevel")){
            userData.soundLevel = PlayerPrefs.GetFloat("soundLevel");
            sound.mute = userData.soundOnOrOff;
            sound.volume = userData.soundLevel;
        }
    }
}
