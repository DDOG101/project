﻿using UnityEngine;
public class playerShipControl : MonoBehaviour {
    public float speed;
    public float tilt;
    public Boundary boundary;
    public GameObject playerShip;
    public Rigidbody projectilePrefab;
    public float projectileSpeed;
    public float FIRE_DELAY = 1f;
    private float nextFireTime = 2f;
    public AudioSource shotSound;
    public AudioClip shotClip;
    public joystick joystick;
    public GameObject joyStickControls, shotBtn;
    void Start () {
        //Screen.orientation = ScreenOrientation.Landscape;
        int w = Screen.width;
        int h = Screen.height;
        Screen.SetResolution(w,h,true);
        joyStickControls.SetActive(false);
        shotBtn.SetActive(false);
    }
	
	
	void Update () {
        if (Time.time > nextFireTime)
        {
            CheckFireKey();
        }
    }

    public void setFireTime(float fireTime) {
        FIRE_DELAY += fireTime;
    }

    public void setMoveSpeed(float moveSpeed)
    {
        speed += moveSpeed;
    }

    void FixedUpdate() {
        if (Application.platform == RuntimePlatform.Android)
        {
            joyStickControls.SetActive(true);
            shotBtn.SetActive(true);
            float moveHorizontal = joystick.inputDirection.x;
            float moveVertical = joystick.inputDirection.z;

            Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
            Rigidbody shipBody = playerShip.GetComponent<Rigidbody>();
            shipBody.velocity = movement * speed;

            shipBody.position = new Vector3
            (
                Mathf.Clamp(shipBody.position.x, boundary.xMin, boundary.xMax),
                0.0f,
                Mathf.Clamp(shipBody.position.z, boundary.zMin, boundary.zMax)
            );
            shipBody.rotation = Quaternion.Euler(shipBody.velocity.z * tilt, 0.0f, 0.0f);
        }
        else
        {
            float moveHorizontal = Input.GetAxis("Horizontal");
            float moveVertical = Input.GetAxis("Vertical");

            Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
            Rigidbody shipBody = playerShip.GetComponent<Rigidbody>();
            shipBody.velocity = movement * speed;

            shipBody.position = new Vector3
            (
                Mathf.Clamp(shipBody.position.x, boundary.xMin, boundary.xMax),
                0.0f,
                Mathf.Clamp(shipBody.position.z, boundary.zMin, boundary.zMax)
            );
            shipBody.rotation = Quaternion.Euler(shipBody.velocity.z * tilt, 0.0f, 0.0f );
        }
    }

    private void CheckFireKey() {
        if (Application.platform != RuntimePlatform.Android && Input.GetButton("Fire1"))
        {
            CreateProjectile();
            nextFireTime = Time.time + FIRE_DELAY;
        } else if (fireButton.pushed==true) {
            CreateProjectile();
            nextFireTime = Time.time + FIRE_DELAY;
        }
    }

    private void CreateProjectile() {
        if (Time.timeScale!=0) {
            Vector3 position = transform.position;
            Quaternion rotation = transform.rotation;
            Rigidbody projectileRigidBody = (Rigidbody)Instantiate(projectilePrefab, position, rotation);
            projectileRigidBody.rotation = Quaternion.Euler(90.0f, 0.0f, 90.0f);

            // create and apply velocity 
            Vector3 projectileVelocity = transform.TransformDirection(Vector3.right * projectileSpeed);
            projectileRigidBody.AddForce(projectileVelocity);

            // destroy object after 5 seconds ...
            GameObject projectileGO = projectileRigidBody.gameObject;
            Destroy(projectileGO, 5f);
            shotSound.PlayOneShot(shotClip, 0.7f);
        }
    }
}

[System.Serializable]
public class Boundary {
    public float xMin, xMax, zMin, zMax;
}
