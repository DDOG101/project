﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerHitsEnemy : MonoBehaviour {
    private gameManager gm;
    public AudioClip enemy;
    public AudioSource sound;
    public Rigidbody playerPrefab;

    void Start() {
        GameObject gameManager = GameObject.FindWithTag("MainCamera");
        gm = gameManager.GetComponent<gameManager>();
    }

    IEnumerator OnTriggerEnter(Collider hit) {
        if (hit.gameObject.tag == ("enemy_1")) {
            gm.UpdateLifeText(-1);
            Destroy(hit.gameObject);
            sound.PlayOneShot(enemy, 0.7f);
            transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
            transform.GetComponent<SphereCollider>().enabled = false;
            Vector3 position = new Vector3(0.0f, 0.0f, 0.0f);
            transform.position = position;
            if (gm.totalLife == 0) {
                gm.GameOver();
            }
            yield return new WaitForSeconds(1);
            transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
            yield return new WaitForSeconds(1);
            transform.GetComponent<SphereCollider>().enabled = true;
            //ToDo Need to fix this!! Spawn in explosion and give player shild
        }

        else if (hit.gameObject.tag == ("enemy_2"))
        {
            gm.UpdateLifeText(-1);
            Destroy(hit.gameObject);
            sound.PlayOneShot(enemy, 0.7f);
            transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
            transform.GetComponent<SphereCollider>().enabled = false;
            Vector3 position = new Vector3(0.0f, 0.0f, 0.0f);
            transform.position = position;
            if (gm.totalLife == 0)
            {
                gm.GameOver();
            }
            yield return new WaitForSeconds(1);
            transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
            yield return new WaitForSeconds(1);
            transform.GetComponent<SphereCollider>().enabled = true;
            //ToDo Need to fix this!! Spawn in explosion and give player shild
        }
    }
}
