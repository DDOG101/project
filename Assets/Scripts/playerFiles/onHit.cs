﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class onHit : MonoBehaviour {
    private gameManager gm;
    public AudioClip enemy;
    public AudioSource sound;

    void Start () {
        GameObject gameManager = GameObject.FindWithTag("MainCamera");
        gm = gameManager.GetComponent<gameManager>();
	}
	
    IEnumerator OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.tag == ("enemy_1")) {
            gm.UpdateScoreText(5);
            Destroy(hit.gameObject);
            sound.PlayOneShot(enemy, 0.7f);
            this.GetComponent<CapsuleCollider>().enabled = false;
            transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
            //Spawn in explosion
            yield return new WaitForSeconds(1);
            Destroy(gameObject,5f);
        }

        else if (hit.gameObject.tag == ("enemy_2"))
        {
            gm.UpdateScoreText(10);
            Destroy(hit.gameObject);
            sound.PlayOneShot(enemy, 0.7f);
            this.GetComponent<CapsuleCollider>().enabled = false;
            transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
            //Spawn in explosion
            yield return new WaitForSeconds(1);
            Destroy(gameObject, 5f);
        }
    }
}