-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: fdb4.biz.nf
-- Generation Time: May 14, 2017 at 03:48 PM
-- Server version: 5.7.18-log
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `1565092_darren`
--

-- --------------------------------------------------------

--
-- Table structure for table `input`
--

CREATE TABLE `input` (
  `id` int(11) NOT NULL,
  `question` varchar(100) NOT NULL,
  `rightAnswer` varchar(100) NOT NULL,
  `tag` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `input`
--

INSERT INTO `input` (`id`, `question`, `rightAnswer`, `tag`) VALUES
(1, '2(?)+2(?)=4', '1', 'math'),
(2, '3(?)+6=12', '2', 'math'),
(3, 'I like to listen to m____', 'music', 'english'),
(4, 'I want to go t____ for my birthday', 'there', 'english'),
(5, 'Jack had t__ much to eat', 'too', 'english'),
(6, '6 % ? = 3', '2', 'math'),
(7, '10 x 2 = ?', '20', 'math'),
(8, 'James is b____', 'bored', 'english');

-- --------------------------------------------------------

--
-- Table structure for table `mcq`
--

CREATE TABLE `mcq` (
  `id` int(100) NOT NULL,
  `question` varchar(100) NOT NULL,
  `answerOne` varchar(100) NOT NULL,
  `answerTwo` varchar(100) NOT NULL,
  `answerThree` varchar(100) NOT NULL,
  `rightAnswer` varchar(100) NOT NULL,
  `tag` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mcq`
--

INSERT INTO `mcq` (`id`, `question`, `answerOne`, `answerTwo`, `answerThree`, `rightAnswer`, `tag`) VALUES
(1, '_____ tooth hurts? What is the missing word for this sentence?', 'witch', 'whcih', 'wtich', 'which', 'english'),
(2, 'I can ___ you. What is the missing word for this sentence?', 'sea', 'sae', 'seen', 'see', 'english'),
(3, 'The girl _____ her hair. What is the missing word for this sentence?', 'die', 'dye', 'dry', 'dyed', 'english'),
(4, 'What is the right way to spell this word?', 'ridim', 'rhythym', 'rythm', 'rhythm', 'english'),
(5, 'James ____ my chocolate bar. What is the missing word for this sentence?', 'tock', 'tuck', 'tcok', 'took', 'english'),
(6, 'It\'s rude to _____ at people. What is the missing word for this sentence?', 'steer', 'stared', 'staring', 'stare', 'english'),
(7, 'What is the right way to spell this name?', 'Dillan', 'Dilan', 'Dillon', 'Dylan', 'english'),
(8, '3 _ 3 = 6 What is the right mathematical sign for this sum?', 'x', '%', '-', '+', 'math'),
(9, '2 _ 2 = 0 What is the right mathematical sign for this sum?', 'x', '%', '+', '-', 'math'),
(10, '2 _ 2 = 1 What is the right mathematical sign for this sum?', 'x', '-', '+', '%', 'math'),
(11, '4 _ 2 = 8 What is the right mathematical sign for this sum?', '%', '-', '+', 'x', 'math'),
(12, 'I went __ the shop with my brother. What is the missing word for this sentence?', 'two', 'too', '2', 'to', 'english');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `score` int(11) NOT NULL,
  `role` int(100) NOT NULL,
  `totalPrecent` double NOT NULL,
  `mathPrecent` double NOT NULL,
  `englishPrecent` int(11) NOT NULL,
  `mathQuestionRight` int(11) NOT NULL,
  `totalMathQuestionsAsked` int(11) NOT NULL,
  `englishQuestionRight` int(11) NOT NULL,
  `totalEnglishQuestionsAsked` int(11) NOT NULL,
  `totalQuestionsRight` int(11) NOT NULL,
  `totalQuestionsAsked` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `score`, `role`, `totalPrecent`, `mathPrecent`, `englishPrecent`, `mathQuestionRight`, `totalMathQuestionsAsked`, `englishQuestionRight`, `totalEnglishQuestionsAsked`, `totalQuestionsRight`, `totalQuestionsAsked`) VALUES
(1, 'admin', 'admin', 'darren@hotmail.com', 800, 2, 0, 0, 0, 0, 0, 0, 1, 0, 1),
(4, 'jrsamson', 'olindan123', 'jhitman26@gmail.com', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 'darren', '123', 'darren@hotmail.com', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `input`
--
ALTER TABLE `input`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mcq`
--
ALTER TABLE `mcq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `input`
--
ALTER TABLE `input`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `mcq`
--
ALTER TABLE `mcq`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
