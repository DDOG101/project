# README #

### What is this repository for? ###
I have made a fun e-learning game that will help children with Dyslexia and Dyscalculia to learn maths and English. This is done by testing them with multiple choice and input style questions. When the user is finished playing the game they can see their results and get an indication to if they are showing signs of the learning disability and how well they are doing. This was programmed in C# and you can download the apk here with the link provided.

* [APK](https://developer.cloud.unity3d.com/share/ZJOKzLZVNG/)
* Status - Finished

### Who do I talk to? ###
* [Find me on linkedin](https://www.linkedin.com/in/darren-cosgrave-49574211a/)
* Email darrencosgrave101@gmail.com